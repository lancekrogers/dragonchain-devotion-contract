import sys
import json
import asyncio
from handler import handler


def get_stdin():
    buffer = ""
    for line in sys.stdin.readline():
        buffer += line
    return buffer


def main():
    data = get_stdin()
    parsed = json.loads(data)

    loop = asyncio.get_event_loop()
    ret = loop.run_until_complete(handler(parsed))
    serialized = json.dumps(ret)
    if ret is not None:
        sys.stdout.write(serialized)
    # loop.close()


if __name__ == "__main__":
    main()
