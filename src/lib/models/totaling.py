import asyncio
from util import Logger


def quipu_obj_prefix(quipuId):
    """
    @param quipuId The quipu ID
    """
    return f"devotions/devoted/{quipuId}"


def total_key(quipuId):
    """
    @param quipuId The quipu ID
    """
    return f"devotions/devoted/{quipuId}/totals"


class TotalsModel:
    def __init__(
        self, heap_memory, quipuId, totals={"drgn": 0, "time": 0, "fees": 0}
    ):
        self.heap_memory = heap_memory
        self.quipuId = quipuId
        self.totals = totals
        self.logger = Logger()

    def save_totals(self):
        self.heap_memory.set(total_key(self.quipuId), self.totals)

    async def generate(self):
        """
        Calculate totals for quipuId and replace existing totals object
        """
        self.logger.info(f"METHOD: Totals.calculate totals: {self.quipuId}")
        devotion_keys = await self.heap_memory.get_all_objects(
            quipu_obj_prefix(self.quipuId)
        )
        devotions = await self.get_all_devotion_objects(devotion_keys)
        totals = {"drgn": 0, "time": 0, "fees": 0}
        for devotion in devotions:
            totals["drgn"] += devotion["drgn"]
            totals["time"] += devotion["time"]
            totals["fees"] += devotion["fee"]
        self.totals = totals
        self.save_totals()

    async def get_all_devotion_objects(self, devotion_keys_list):
        """
        @param devotion_keys_list List of devotion keys

        Given a list of devotion keys in the format "/path/to/obj",
        get all objects associated with the keys
        """
        self.logger.info("METHOD: TotalsModel.get_all_devotion_objects")

        async def fetch_object(key):
            result = await self.heap_memory.get(key[1:])
            return result

        coros = [fetch_object(key) for key in devotion_keys_list]
        return await asyncio.gather(*coros)

    async def get_totals(self):
        """
        Get totals by quipuId
        """
        key = total_key(self.quipuId)
        totals = await self.heap_memory.get(key)
        self.logger.info(f"METHOD TotalsModel.get_totals :" f" {self.quipuId}")
        if totals in (0, {}):
            return None
        return totals

    async def add(self, drgn, time, fee):
        """
        Add to existing totals objct
        """
        self.logger.info(f"METHOD: TotalsModel.add: adding to {self.quipuId}")
        totals = await self.get_totals()
        if totals:
            self.totals = totals
        self.totals["drgn"] += drgn
        self.totals["time"] += time
        self.totals["fees"] += fee
        self.save_totals()
