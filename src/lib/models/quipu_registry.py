"""
Register unique data strings

Unique data must be registered
"""
from hashlib import sha3_256
from util import Logger


REGISTRY_KEY = "quipu/registry"


class QuipuRegistryModel:
    def __init__(self, heap_memory):
        self.heap_memory = heap_memory
        self.registry = {}
        self.logger = Logger()

    async def load_registry(self):
        """
        Load quipuId registry from dragonchain into memory
        """
        on_chain_registry = await self.heap_memory.get(REGISTRY_KEY)
        if on_chain_registry not in (0, {}):
            self.logger.info("Loading quipuId registry")
            self.registry = on_chain_registry
        else:
            self.logger.info("No quipu registry found")

    def save_registry(self):
        self.heap_memory.set(REGISTRY_KEY, self.registry)

    def check_availability(self, key):
        try:
            self.registry[key]
            return False
        except KeyError:
            return True

    async def register(self, quipuId):
        """
        Attempt to register the quipuId
        log an error if it is already registered
        quipuIds will be normalized to lowercase
        """
        await self.load_registry()
        quipuId = quipuId.lower()
        if not self.check_availability(quipuId):
            self.logger.error(f"quipuId {quipuId} is already registered")
            return {"OUTPUT_TO_CHAIN": False}  # Does not output to chain
        else:
            self.registry[quipuId] = self.hash_quipuId(quipuId)
            self.registry[self.hash_quipuId(quipuId)] = quipuId
            self.save_registry()
            self.logger.info("QuipuId registry has been saved to heap_memory")

    def hash_quipuId(self, quipuId):
        return f"0x{sha3_256(quipuId.encode()).hexdigest()}"
