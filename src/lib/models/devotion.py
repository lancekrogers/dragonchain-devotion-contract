from math import floor
from hashlib import sha3_256
from util import Logger
from util.settings import ETH_DRGN_FEE
from lib.wrappers import BurnApi, PartyApi, PublisherContract
from util.constants import DEVOTION_ABI
from util.abi_tools import encode_contract_data, bytes32
from .quipu_registry import QuipuRegistryModel
from .totaling import TotalsModel


def registered_key(address):
    return f"devotions/registered/{address}"


def devoted_key(quipuId, address):
    """
    @param quipuId The quipu ID
    @param address The devotion address
    """
    return f"devotions/devoted/{quipuId}/{address}"


def normalize_numbers(value, decimal=18):
    if type(value) == str:
        try:
            value = int(value)
        except ValueError:
            value = float(value)
    if type(value) == int:
        return value
    elif type(value) == float:
        value_str = str(value)
        split = value_str.split(".")
        if len(split) > 1:
            before, after = split
        else:
            return int(float(value))
        if len(after) < decimal:
            for val in range(decimal - len(after)):
                after = after + "0"
        elif len(after) > decimal:
            after = after[:18]
        return int(before + after)


class DevotionModel:
    def __init__(
        self,
        heap_memory,
        ethereumTxnId=None,
        quipuId="general",
        address=None,
        drgn=0,
        time=0,
        returnTokensReceiptId="",
        fee=ETH_DRGN_FEE,
        event_log=[],
    ):
        self.heap_memory = heap_memory
        self._ethereumTxnId = ethereumTxnId
        self._quipuId = quipuId
        self._quipuHash = ""
        self._address = address
        self._drgn = drgn
        self._time = time
        self._fee = fee
        self._returnTokensReceiptId = returnTokensReceiptId
        self.event_log = event_log
        self.logger = Logger()

    # ------------------------------------------
    # Properties
    # ------------------------------------------

    @property
    def address(self):
        return self._address.lower()

    @address.setter
    def address(self, value):
        self._address = value.lower()

    @property
    def ethereumTxnId(self):
        return self._ethereumTxnId.lower()

    @ethereumTxnId.setter
    def ethereumTxnId(self, value):
        self._ethereumTxnId = value

    @property
    def quipuId(self):
        return self._quipuId.lower()

    @quipuId.setter
    def quipuId(self, value):
        self._quipuId = value

    @property
    def quipuHash(self):
        """
        Sets and returns the keccak hash of the quipuId
        """
        if self._quipuHash == "":
            self._quipuHash = sha3_256(self.quipuId.encode()).hexdigest()
        return f"0x{self._quipuHash}"

    @property
    def drgn(self, decimal=18):
        return normalize_numbers(self._drgn)

    @drgn.setter
    def drgn(self, value):
        self._drgn = value

    @property
    def time(self):
        return normalize_numbers(self._time, decimal=0)

    @time.setter
    def time(self, value):
        self._time = value

    @property
    def fee(self):
        return normalize_numbers(self._fee)

    @fee.setter
    def fee(self, value):
        self._fee = value

    @property
    def returnTokensReceiptId(self):
        return self._returnTokensReceiptId.lower()

    @returnTokensReceiptId.setter
    def returnTokensReceiptId(self, value):
        self._returnTokensReceiptId = value

    # ------------------------------------------
    # Methods
    # ------------------------------------------

    def save_registered(self):
        self.heap_memory.set(
            registered_key(self.address), self.registered_as_dict()
        )

    def save_devoted(self):
        self.heap_memory.set(
            devoted_key(self.quipuId, self.address), self.devoted_as_dict()
        )

    def registered_as_dict(self):
        self_dict = {
            "address": self.address,
            "drgn": self.drgn,
            "time": self.time,
            "fee": self.fee,
            "quipuId": self.quipuId,
        }
        return self_dict

    def devoted_as_dict(self):
        self_dict = {
            "ethereumTxnId": self.ethereumTxnId,
            "quipuId": self.quipuId,
            "address": self.address,
            "drgn": self.drgn,
            "time": self.time,
            "fee": self.fee,
            "returnTokensReceiptId": self.returnTokensReceiptId,
        }
        return self_dict

    async def register_devotion(self, timecheck=True):
        """
        Creates a new registered devotion
        """
        available_time = await PartyApi.get_available_time(self.address)
        if timecheck:
            self.logger.info(
                f"METHOD Devotion.register_devotion timecheck {available_time}"
            )
            if floor(available_time) < self.time:
                msg = "This account does not have enough available time"
                " to devote"
                self.logger.error(msg)
                self.heap_memory.set('error', msg)
                return
        burntokens = await BurnApi.tokens_for_time(self.address, self.time)
        if burntokens > -1:
            tokens_needed = normalize_numbers(burntokens) + self.fee
            self.drgn = tokens_needed
            self.logger.info(
                f"tokens_needed: type: {type(tokens_needed)} "
                f"value: {tokens_needed}"
            )
            registered_obj = {
                "address": self.address,
                "drgn": f"{tokens_needed}",
                "time": f"{self.time}",
                "fee": f"{self.fee}",
                "quipuId": f"{self.quipuId}",
            }
            self.heap_memory.set(registered_key(self.address), registered_obj)
        else:
            msg = "This account does not have enough time available to burn"
            self.logger.error(msg)
            self.heap_memory.set('error', msg)
            return

    async def cancel_devotion(self):
        """
        Cancel registered or errored devotions
        """
        key = registered_key(self.address)
        self.logger.info(f"METHOD Devotion.cancel_devotion key -> ${key}")
        registered_devotion = await DevotionModel.get_registered_devotion(
            self.heap_memory, self.address
        )
        if registered_devotion:
            # Sets heap object to an empty object {}
            msg = "Canceling devotion"
            self.logger.info(msg)
            self.heap_memory.set('success', msg)
            self.heap_memory.set(key, {})
        else:
            self.heap_memory.set('error',
                                 'No devotion found for this address')

    async def finalize_devotion(self):
        """
        Finalizes devotion, submits devotion to ethereum contract.
        """
        self.logger.info("METHOD finalized_devotion starting...")
        # Check for matching registered devotion
        registered = await self.get_registered_devotion(
            self.heap_memory, self.address
        )

        # if match, trigger publisher contract
        if registered:
            self.logger.info(
                f"Method finalize_devotion -> registered\
                devotion found for {self.address}"
            )
            registered_dict = registered.registered_as_dict()
            registered_drgn = registered_dict["drgn"]
            self.logger.info(
                f"registed: {registered_drgn} type: "
                f"{type(registered_drgn)}"
            )
            self.logger.info(f"self.drgn: {self.drgn} type: {type(self.drgn)}")

            if registered_drgn != self.drgn:
                self.logger.info(
                    f"registered_drgn: {registered_drgn} "
                    f"self.drgn {self.drgn}"
                )
                self.logger.error(
                    "Registered devotion doesn't match coins sent. "
                    "Returning coins to sender"
                )
                # Trigger publisher to return coins to sender minus fee
                if self.drgn >= self.fee:
                    data = encode_contract_data(
                        DEVOTION_ABI,
                        "returnTokens",
                        self.address,
                        self.drgn,
                        self.fee,
                    )
                    self.logger.info(f"Returning coins to sender {data}.")
                    await PublisherContract.create_transaction(
                        self.heap_memory, data
                    )
                else:
                    self.logger.info(
                        f"Coins sent is less than FEE, not returning tokens"
                    )
                return {"OUTPUT_TO_CHAIN": False}  # Does not output to chain
            self.quipuId = registered_dict["quipuId"]
            self.time = registered_dict["time"]
            self.fee = registered_dict["fee"]

            # save finalized devotion
            self.save_devoted()
            # cancel registered devotion
            await self.cancel_devotion()
            # Trigger publisher contract to finalize devotion

            data = encode_contract_data(
                DEVOTION_ABI,
                "registerDevotion",
                self.address,
                bytes32(int(self.ethereumTxnId, 16)),
                self.drgn,
                self.time,
                self.fee,
                bytes32(int(self.quipuHash, 16)),
            )
            self.logger.info(data)
            await PublisherContract.create_transaction(self.heap_memory, data)

            self.logger.info("Devotion is being published to ethereum")

            await self.update_totals()
        else:
            # If no match, return coins to sender minus fee
            self.logger.info(
                "No matching devotion found. " "Returning coins to sender"
            )
            # Trigger publisher to return coins to sender minus fee
            if self.drgn >= self.fee:
                data = encode_contract_data(
                    DEVOTION_ABI,
                    "returnTokens",
                    self.address,
                    self.drgn,
                    self.fee,
                )
                self.logger.info(f"Returning coins to sender {data}.")
                await PublisherContract.create_transaction(
                    self.heap_memory, data
                )
            else:
                self.logger.info("Sent DRGN are less than fee, keeping tokens")
            # trigger publisher to return coins to sender
            return {"OUTPUT_TO_CHAIN": False}  # Does not output to chain

    async def update_receipt(self, returnTokensReceiptId, input_data):
        """
        Adds receipt transaction to the devoted object
        """
        self.logger.info("METHOD: DevotionModel.update_receipt, starting...")
        ethereumTxnId = input_data["_txnid"]
        address = input_data["_addr"]
        quipuHash = input_data["_data"]
        quipu_registry = QuipuRegistryModel(self.heap_memory)
        await quipu_registry.load_registry()
        registry = quipu_registry.registry
        try:
            quipuId = registry[quipuHash]
        except KeyError:
            self.logger.error(
                f"ERROR: {quipuHash} is not a registered quipuId"
            )
            return {"OUTPUT_TO_CHAIN": False}  # Does not output to chain
        finalized_devotion = await self.get_finalized_devotion(
            self.heap_memory, quipuId, address
        )
        if finalized_devotion:
            if finalized_devotion.ethereumTxnId == ethereumTxnId.lower():
                finalized_devotion.returnTokensReceiptId = (
                    returnTokensReceiptId
                )
            finalized_devotion.save_devoted()
            self.logger.info(
                f"Receipt added: {finalized_devotion.devoted_as_dict()}"
            )
        else:
            self.logger.error(
                f"ERROR: No matching devotion found for "
                f"input data {input_data}"
            )
            return {"OUTPUT_TO_CHAIN": False}  # Does not output to chain

    async def update_totals(self):
        """
        Update quipu devtion totals
        """
        self.logger.info(f"METHOD: Devtotion.update_totals {self.quipuId}")
        totals = TotalsModel(self.heap_memory, self.quipuId)
        await totals.add(self.drgn, self.time, self.fee)

    @staticmethod
    async def get_registered_devotion(heap_memory, address):
        """
        Get registered devotion by address

        returns a devotion object or None if devotion not found
        """
        logger = Logger()
        key = registered_key(address)
        registered_dev = await heap_memory.get(key)
        logger.info(
            f"METHOD DevotionModel.get_registered_devotion:"
            f" {registered_dev}"
        )
        if registered_dev in (0, {}):
            return None
        return DevotionModel(heap_memory, **registered_dev)

    @staticmethod
    async def get_finalized_devotion(heap_memory, quipuId, address):
        """
        Get devotion/s by tran_id, address, quipuId or fee_address

        returns a devotion object or a list of devotion objects
        """
        logger = Logger()
        key = devoted_key(quipuId, address)
        finalized_dev = await heap_memory.get(key)
        logger.info(
            f"METHOD DevotionModel.get_finalized_devotion:" f" {finalized_dev}"
        )
        if finalized_dev in (0, {}):
            return None
        return DevotionModel(heap_memory, **finalized_dev)
