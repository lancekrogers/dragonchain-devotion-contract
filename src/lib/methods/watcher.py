from util import Logger
import codecs
from util.settings import DEVOTION_CONTRACT_ADDRESS
from util.abi_tools import decode_input_data, HexBytes
from util.constants import DEVOTION_ABI


class Watcher:

    logger = Logger()
    decode_hex = codecs.getdecoder("hex_codec")

    def __init__(self, payload):
        self.payload = payload
        self.decoded_transactions = []

    @staticmethod
    def decode_transaction(transaction):
        """
        Decodes a transaction object and returns relevant transaction info

        transactionHash ethereumTxnId
        topic 0: hash of event signature
        topic 1: from address
        topic 2: to address
        data: amount of drgns transfered
        """
        Watcher.logger.info("Watcher.decode_transaction starting...")
        try:
            topics = transaction["topics"]
            data = transaction["data"]
            eth_txn_id = transaction["transactionHash"]
            from_address = f"0x{topics[1][26:]}"
            to_address = f"0x{topics[2][26:]}"
            value = int.from_bytes(Watcher.decode_hex(data[2:])[0], "big")
            Watcher.logger.info(
                "Watcher.decode_transaction token transfer transaction decoded"
            )
            return {
                "type": "tokenTransaction",
                "ethereumTxnId": eth_txn_id,
                "fromAddress": from_address,
                "toAddress": to_address,
                "drgnSent": value,
            }
        except KeyError:
            Watcher.logger.info(
                "Watcher.decode_transaction contract invocation"
            )
            eth_txn_id = transaction["hash"]
            input_data = transaction["input"]
            from_address = transaction["from"]
            input_function_abi, input_data = decode_input_data(
                DEVOTION_ABI, input_data
            )
            function_name = input_function_abi["name"]
            input_data["_txnid"] = HexBytes(input_data["_txnid"]).hex()
            input_data["_data"] = HexBytes(input_data["_data"]).hex()
            return {
                "type": "contractInvocation",
                "ethereumTxnId": eth_txn_id,
                "fromAddress": from_address,
                "functionName": function_name,
                "inputData": input_data,
            }

    def decode_payload(self):
        """
        Converts watcher payload transactions to list of decoded transactions
        """
        self.decoded_transactions = []
        for transaction in self.payload:
            decoded_transaction = Watcher.decode_transaction(transaction)
            if decoded_transaction["type"] == "tokenTransaction":
                if (
                    decoded_transaction.get("toAddress").lower()
                    == DEVOTION_CONTRACT_ADDRESS.lower()
                ):
                    self.decoded_transactions.append(decoded_transaction)
            elif decoded_transaction["type"] == "contractInvocation":
                self.decoded_transactions.append(decoded_transaction)

    def get_decoded_transactions(self):
        if not self.decoded_transactions:
            self.decode_payload()
        return self.decoded_transactions
