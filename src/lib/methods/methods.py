from lib.models import DevotionModel, QuipuRegistryModel, TotalsModel
from lib.methods.watcher import Watcher

# from lib.schemas import SchemaValidator


# Devotion Methods
async def register(heap_memory, parameters):
    """
    Creates a new pending devotion
    parameters should contain address, time and quipu_id
    """
    # SchemaValidator.is_valid_register_schema(parameters)
    if parameters["quipuId"] == "":
        quipuId = "general"
    else:
        quipuId = parameters["quipuId"]
    devotion = DevotionModel(
        heap_memory,
        address=parameters["address"],
        time=parameters["time"],
        quipuId=quipuId,
    )
    await devotion.register_devotion(timecheck=True)
    return True


async def cancel(heap_memory, parameters):
    """
    Cancels a registered devotion. Finalized devotions cannot be cancelled
    """
    devotion = DevotionModel(heap_memory, address=parameters["address"])
    await devotion.cancel_devotion()
    return True


async def devote(heap_memory, parameters):
    """
    Triggers publisher contract which will register the devotion with the
    ethereum devotion contract

    Searches for pending transactions that match the address and coin amount
    in the ethereum transaction
    """
    devotion = DevotionModel(
        heap_memory,
        address=parameters["address"],
        drgn=int(parameters["drgn"]),
        ethereumTxnId=parameters["ethereumTxnId"],
    )

    await devotion.finalize_devotion()

    return True


async def add_receipt(heap_memory, parameters):
    """
    Adds returnTokenReceipt to the devoted object in the heap
    """
    devotion = DevotionModel(heap_memory)

    await devotion.update_receipt(
        parameters["ethereumTxnId"], parameters["inputData"]
    )

    return True


async def watcher(heap_memory, parameters):
    """
    Receives response from watcher, parses data and calls devote for each
    transaction
    """
    watcher = Watcher(parameters)
    transactions = watcher.get_decoded_transactions()

    for transaction in transactions:
        if transaction["type"] == "tokenTransaction":
            params = {
                "address": transaction["fromAddress"],
                "drgn": transaction["drgnSent"],
                "ethereumTxnId": transaction["ethereumTxnId"],
            }
            await devote(heap_memory, params)
        elif transaction["type"] == "contractInvocation":
            if transaction["functionName"] == "registerDevotion":
                # add receipt to devotion
                params = {
                    "ethereumTxnId": transaction["ethereumTxnId"],
                    "inputData": transaction["inputData"],
                }
                await add_receipt(heap_memory, params)
            else:
                Watcher.logger.info(
                    f"Devotion DC contract is ignoring "
                    f"{transaction['functionName']}"
                    f" sent to Devotion Ethereum contract."
                    f" Don't worry, this is normal."
                )
    return True


async def claim_quipu_id(heap_memory, parameters):
    """
    Checks for a quipu id and registers it if it has not been registered
    """
    quipu_registry = QuipuRegistryModel(heap_memory)
    requested_quipu_id = parameters["quipuId"]

    await quipu_registry.register(requested_quipu_id)
    return True


async def generate_totals(heap_memory, parameters):
    """
    Totals all devotions for a given quipuId
    """
    total_obj = TotalsModel(heap_memory, parameters["quipuId"])
    await total_obj.generate()
    return True
