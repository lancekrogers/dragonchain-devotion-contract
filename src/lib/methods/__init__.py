from .methods import (
    register,
    cancel,
    devote,
    watcher,
    claim_quipu_id,
    generate_totals,
)
