VALID_METHODS = ["register", "devote", "cancel", "watcher", "claim_quipu_id"]

event_schema = {
    "type": "object",
    "properties": {"payload": {"type": "object"}},
    "required": ["payload"],
}

register_schema = {
    "type": "object",
    "properties": {
        "address": {"type": "string"},
        "time": {"type": "string"},
        "quipuId": {"type": "string"},
    },
    "required": ["address", "time", "quipuId"],
}
