from jsonschema import validate
from util import FailureByDesign
from .schemas import event_schema, register_schema


def validate_schema(thing_to_validate, schema, error_source):
    if not validate(instance=thing_to_validate, schema=schema):
        raise FailureByDesign("INVALID_SCHEMA", error_source)


class SchemaValidator:
    @staticmethod
    def is_valid_event_payload(event_payload):
        return validate_schema(event_payload, event_schema, "EVENT_PAYLOAD")

    @staticmethod
    def is_valid_register_schema(register_payload):
        return validate_schema(
            register_payload, register_schema, "REGISTER_PAYLOAD"
        )
