from util.settings import BURN_API_ENDPOINT
from util import Logger
import httpx


class BurnApi:
    @staticmethod
    async def tokens_for_time(address, time):
        logger = Logger()
        endpoint = f"{BURN_API_ENDPOINT}/burn/{address}/{time}"
        async with httpx.AsyncClient() as client:
            req = await client.get(endpoint)
        if req.status_code == 200:
            return req.json()["tokens"]
        else:
            logger.error(
                f"Invalid response from burn api: ENDPOINT: {endpoint}"
            )
            return {"OUTPUT_TO_CHAIN": False}  # Does not output to chain
