"""
Use the party api to check the address the user submitted along with their
available time so you can return the amount of DRGNS that need to be sent
to devote the time.

If ETHEREUM_NETWORK is set to ETH_ROPSTEN,

"""
from util.settings import PARTY_API_ENDPOINT, ETHEREUM_NETWORK
from util.testnet_time import calculate_score
from util import Logger
import httpx


class PartyApi:
    @staticmethod
    async def get_available_time(address):
        logger = Logger()
        if ETHEREUM_NETWORK == "ETH_ROPSTEN":
            score = await calculate_score(address)
            return score["adjustedScore"]
        else:
            endpoint = f"{PARTY_API_ENDPOINT}/score/{address}"
            async with httpx.AsyncClient() as client:
                req = await client.get(endpoint)
            if req.status_code == 200:
                return req.json()["adjustedScore"]
            else:
                logger.error(
                    f"Invalid response from party api: ENDPOINT:" f"{endpoint}"
                )
                return {"OUTPUT_TO_CHAIN": False}  # Does not output to chain
