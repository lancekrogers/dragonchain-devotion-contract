from util.settings import PUBLISHER_TRANSACTION_TYPE, DEVOTION_CONTRACT_ADDRESS
from util import Logger, FailureByDesign


class PublisherContract:
    @staticmethod
    async def create_transaction(heap_memory, data):
        """
        @param heap_memory HeapMemory object
        @param data hex encoded input_data
        """
        logger = Logger()
        client = heap_memory.dragonchain
        payload = {"data": data, "to": DEVOTION_CONTRACT_ADDRESS}
        logger.info(
            f"PublisherContract.create_transaction publishing payload:"
            f" {payload}"
        )

        response = await client.create_transaction(
            PUBLISHER_TRANSACTION_TYPE, payload
        )
        if response["ok"]:
            logger.info("PublisherContract.create_transaction success")
            return True
        else:
            logger.error("PublisherContract.create_transaction failed")
            logger.error(f"{response}")
            raise FailureByDesign("PUBLISHER_FAILURE", str(response))
