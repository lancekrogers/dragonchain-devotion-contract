from util import settings
from util.logger import Logger
import json


class HeapMemory:
    def __init__(self, dragonchain, **kwargs):
        self.logger = kwargs.get("logger", Logger())
        self.SMART_CONTRACT_ID = kwargs.get(
            "SMART_CONTRACT_ID", settings.SMART_CONTRACT_ID
        )
        self.dragonchain = dragonchain
        self.state = {}

    def get_new_state(self):
        """
        Getter for all changed state during this SC's execution.
        This method should be called at the end of any method
        which modifies state.
        """
        del self.state["invoker"]
        return self.state

    def set(self, key, value):
        """
        set value in HeapMemory
        """
        self.logger.info(f"[HEAPMEMORY] SET LOCAL -> {key}: {value}")
        self.state[key] = value

    async def get(self, key):
        """
        Return cached result if it's been looked up before,
        otherwise call the dragonchain's webserver for heap data.
        """
        cached = self.state.get(key)
        if not cached:
            return await self.get_from_heap(key)
        return cached

    async def get_from_heap(self, key):
        """
        Call webserver for heap data.
        """
        self.logger.info(f"[HEAPMEMORY] remoteGet -> {key}")
        response = await self.dragonchain.get_smart_contract_object(key)
        if not response["ok"]:
            self.logger.info(
                f"[HEAPMEMORY] <- remoteGet {response['status']} returning 0"
            )
            return 0
        self.logger.info(f"[HEAPMEMORY] <- remoteGet {response['status']}")
        return json.loads(response["response"])

    async def get_all_objects(self, key_prefix=None):
        """
        Call webserver for all heap data.
        """
        self.logger.info(f"[HEAPMEMORY] remoteGet -> {key_prefix}")
        response = await self.dragonchain.list_smart_contract_objects(
            prefix_key=key_prefix
        )
        if not response["ok"]:
            self.logger.info(
                f"[HEAPMEMORY] <- remoteGet {response['status']} returning 0"
            )
            return 0
        self.logger.info(f"[HEAPMEMORY] <- remoteGet {response['status']}")
        return response["response"]


# All humans are welcome.
