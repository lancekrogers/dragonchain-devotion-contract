import dragonchain_sdk
from util.errors import FailureByDesign
from util import settings
from util.logger import Logger


class DragonchainClient:
    def __init__(self, **kwargs):
        self.logger = kwargs.get("logger", Logger())
        self.SMART_CONTRACT_ID = kwargs.get(
            "SMART_CONTRACT_ID", settings.SMART_CONTRACT_ID
        )
        self.DRAGONCHAIN_ID = kwargs.get(
            "DRAGONCHAIN_ID", settings.DRAGONCHAIN_ID
        )
        self.AUTH_KEY = kwargs.get("AUTH_KEY", settings.AUTH_KEY)
        self.AUTH_KEY_ID = kwargs.get("AUTH_KEY_ID", settings.AUTH_KEY_ID)
        self.ENDPOINT = kwargs.get("ENDPOINT", settings.ENDPOINT)
        self.dragonchain_client = None

    async def initialize(self):
        self.dragonchain_client = await dragonchain_sdk.create_aio_client(
            dragonchain_id=self.DRAGONCHAIN_ID,
            auth_key_id=self.AUTH_KEY_ID,
            auth_key=self.AUTH_KEY,
            endpoint=self.ENDPOINT,
        )

    async def get_smart_contract_secret(self, secret_name):
        try:
            secret = await self.dragonchain_client.get_smart_contract_secret(
                secret_name
            )
            return secret
        except Exception as e:
            self.logger.error(e)
            raise FailureByDesign(
                "DRAGONCHAIN_UNEXPECTED_ERROR", "An unexpected error occured"
            )

    async def get_smart_contract_object(self, key):
        try:
            call = await self.dragonchain_client.get_smart_contract_object(
                key, self.SMART_CONTRACT_ID
            )
            return call
        except Exception as e:
            self.logger.error(e)
            return {"ok": False, "status": 500}

    async def get_public_blockchain_address(self):
        try:
            call = (
                await self.dragonchain_client.get_public_blockchain_addresses()
            )
            return call
        except Exception as e:
            self.logger.error(e)
            return {"ok": False, "status": 500}

    async def create_transaction(self, transaction_type, payload):
        try:
            call = await self.dragonchain_client.create_transaction(
                transaction_type, payload
            )
            return call
        except Exception as e:
            self.logger.error(e)
            raise FailureByDesign(
                "DRAGONCHAIN_UNEXPECTED_ERROR", "An unexpected error occured"
            )

    async def create_bulk_transaction(self, transaction_list):
        try:
            call = await self.dragonchain_client.create_bulk_transaction(
                transaction_list
            )
            return call
        except Exception as e:
            self.logger.error(e)
            raise FailureByDesign(
                "DRAGONCHAIN_UNEXPECTED_ERROR", "An unexpected error occured"
            )

    async def list_smart_contract_objects(self, prefix_key=None):
        try:
            call = await self.dragonchain_client.list_smart_contract_objects(
                prefix_key, self.SMART_CONTRACT_ID
            )
            return call
        except Exception as e:
            self.logger.error(e)
            return {"ok": False, "status": 500}


# All humans are welcome.
