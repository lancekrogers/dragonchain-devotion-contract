from hexbytes import HexBytes
from eth_abi import is_encodable, encode_single
from eth_utils import to_bytes, function_abi_to_4byte_selector
from util.decode_input import decode_function_input


def get_abi_functions(abi):
    return {x["name"]: x for x in abi if x["type"] == "function"}


def get_abi_events(abi):
    return [x for x in abi if x["type"] == "event"]


def get_abi_constructor(abi):
    return [x for x in abi if x["type"] == "constructor"][0]


def bytes32(val):
    if isinstance(val, int):
        result = to_bytes(val)
    else:
        raise TypeError("val %r could not be converted to bytes")
    return result.rjust(32, b"\0")


def str_to_bytes(s):
    """
    Convert 0xHexString to bytes
    :param s: 0x hexstring
    :return:  byte sequence
    """
    return bytes.fromhex(s.replace("0x", ""))


def type_string(type_list):
    start = "("
    end = ")"
    types = ""
    for i, val in enumerate(type_list):
        if i > 0:
            types = types + f",{val}"
        else:
            types = val
    return start + types + end


def encode_contract_data(abi, method, *args):
    """
    Uses ethereum abi to properly encode contract method call and input

    @param abi Ethereum ABI
    @param method contract method
    @args arguments to be passed to the contract method
    """
    function = get_abi_functions(abi)[method]
    inputs = function["inputs"]
    encode_types = []
    encode_values = args
    if len(args) != len(inputs):
        raise Exception("Incorrect number of arguments")
    for num, val in enumerate(inputs):
        if not is_encodable(val["type"], args[num]):
            raise Exception(
                f"Cannot encode, argument {args[num]} should be of type"
                / "{val['type']}."
            )
        encode_types.append(val["type"])
    return HexBytes(
        function_abi_to_4byte_selector(function)
        + encode_single(type_string(encode_types), encode_values)
    ).hex()


def decode_input_data(abi, input_data):
    """
    Uses ethereum abi to decode input data from an Ethereum transaction
    """
    return decode_function_input(abi, input_data)
