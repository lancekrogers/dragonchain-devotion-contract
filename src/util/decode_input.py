from hexbytes import HexBytes
from eth_utils import (
    encode_hex,
    decode_hex,
    function_abi_to_4byte_selector,
    to_bytes,
    remove_0x_prefix,
    add_0x_prefix,
    is_string,
    is_hex,
    is_text,
)
from eth_utils.curried import is_bytes, to_text
from eth_utils.abi import collapse_if_tuple
from eth_abi.codec import ABICodec
from eth_utils.toolz import curry, partial, pipe
from eth_abi.registry import BaseEquals, registry as default_registry
from eth_abi import encoding, decoding
from eth_typing import TypeStr
from eth_abi.grammar import ABIType, TupleType, parse
import functools
import itertools
import binascii
import warnings
from typing import Any, Optional, Union
from collections import namedtuple
import threading
from collections.abc import Iterable, Mapping


def pad_hex(value, bit_size):
    """
    Pads a hex string up to the given bit_size
    """
    value = remove_0x_prefix(value)
    return add_0x_prefix(value.zfill(int(bit_size / 4)))


def hexstr_if_str(to_type, hexstr_or_primitive):
    """
    Convert to a type, assuming that strings can be only hexstr
        (not unicode text)

    @param to_type is a function that takes the arguments
            (primitive, hexstr=hexstr, text=text),
        eg~ to_bytes, to_text, to_hex, to_int, etc
    @param text_or_primitive in bytes, str, or int.
    """
    if isinstance(hexstr_or_primitive, str):
        (primitive, hexstr) = (None, hexstr_or_primitive)
        if remove_0x_prefix(hexstr) and not is_hex(hexstr):
            raise ValueError(
                "when sending a str, it must be a hex string. Got: {0!r}"
                "".format(hexstr_or_primitive)
            )
    else:
        (primitive, hexstr) = (hexstr_or_primitive, None)
    return to_type(primitive, hexstr=hexstr)


def to_4byte_hex(hex_or_str_or_bytes):
    size_of_4bytes = 4 * 8
    byte_str = hexstr_if_str(to_bytes, hex_or_str_or_bytes)
    if len(byte_str) > 4:
        raise ValueError(
            "expected value of size 4 bytes. Got: %d bytes" % len(byte_str)
        )
    hex_str = encode_hex(byte_str)
    return pad_hex(hex_str, size_of_4bytes)


def filter_by_type(_type, contract_abi):
    return [abi for abi in contract_abi if abi["type"] == _type]


def find_functions_by_identifier(contract_abi, callable_check):
    fns_abi = filter_by_type("function", contract_abi)
    return [fn_abi for fn_abi in fns_abi if callable_check(fn_abi)]


def get_abi_input_names(abi):
    if "inputs" not in abi and abi["type"] == "fallback":
        return []
    else:
        return [arg["name"] for arg in abi["inputs"]]


def get_function_by_identifier(fns, identifier):
    if len(fns) > 1:
        raise ValueError(
            "Found multiple functions with matching {0}. "
            "Found: {1!r}".format(identifier, fns)
        )
    elif len(fns) == 0:
        raise ValueError(
            "Could not find any function with matching {0}".format(identifier)
        )
    return fns[0]


def get_function_by_selector(abi, selector):
    def callable_check(fn_abi):
        return encode_hex(
            function_abi_to_4byte_selector(fn_abi)
        ) == to_4byte_hex(selector)

    fns = find_functions_by_identifier(abi, callable_check)
    return get_function_by_identifier(fns, "selector")


class AddressEncoder(encoding.AddressEncoder):
    @classmethod
    def validate_value(cls, value):
        super().validate_value(value)


class combomethod:
    def __init__(self, method):
        self.method = method

    def __get__(self, obj=None, objtype=None):
        @functools.wraps(self.method)
        def _wrapper(*args, **kwargs):
            if obj is not None:
                return self.method(obj, *args, **kwargs)
            else:
                return self.method(objtype, *args, **kwargs)

        return _wrapper


class AcceptsHexStrEncoder(encoding.BaseEncoder):
    def __init__(self, subencoder):
        self.subencoder = subencoder

    @property
    def is_dynamic(self):
        return self.subencoder.is_dynamic

    @classmethod
    def from_type_str(cls, abi_type, registry):
        subencoder_cls = cls.get_subencoder_class()
        subencoder = subencoder_cls.from_type_str(abi_type, registry)
        return cls(subencoder)

    @classmethod
    def get_subencoder_class(cls):
        if cls.subencoder_cls is None:
            raise AttributeError(f"No subencoder class is set. {cls.__name__}")
        return cls.subencoder_cls

    @combomethod
    def validate_value(self, value):
        normalized_value = self.validate_and_normalize(value)
        return self.subencoder.validate_value(normalized_value)

    def encode(self, value):
        normalized_value = self.validate_and_normalize(value)
        return self.subencoder.encode(normalized_value)

    def validate_and_normalize(self, value):
        raw_value = value
        if is_text(value):
            try:
                value = decode_hex(value)
            except binascii.Error:
                self.invalidate_value(
                    value, msg=f"{value} is an invalid hex string"
                )
            else:
                if raw_value[:2] != "0x":
                    if self.is_strict:
                        self.invalidate_value(
                            raw_value,
                            msg="hex string must be prefixed with 0x",
                        )
                    elif raw_value[:2] != "0x":
                        warnings.warn(
                            "in v6 it will be invalid to pass a hex string "
                            'without the "0x" prefix',
                            category=DeprecationWarning,
                        )
        return value


class BytesEncoder(AcceptsHexStrEncoder):
    subencoder_cls = encoding.BytesEncoder
    is_strict = False


class ByteStringEncoder(AcceptsHexStrEncoder):
    subencoder_cls = encoding.ByteStringEncoder
    is_strict = False


class TextStringEncoder(encoding.TextStringEncoder):
    @classmethod
    def validate_value(cls, value):
        if is_bytes(value):
            try:
                value = to_text(value)
            except UnicodeDecodeError:
                cls.invalidate_value(
                    value, msg="not decodable as unicode string"
                )

        super().validate_value(value)


class ABITypedData(namedtuple("ABITypedData", "abi_type, data")):
    """
    This class marks data as having a certain ABI-type.

    >>> a1 = ABITypedData(['address', addr1])
    >>> a2 = ABITypedData(['address', addr2])
    >>> addrs = ABITypedData(['address[]', [a1, a2]])

    You can access the fields using tuple() interface, or with
    attributes:

    >>> assert a1.abi_type == a1[0]
    >>> assert a1.data == a1[1]

    Unlike a typical `namedtuple`, you initialize with a single
    positional argument that is iterable, to match the init
    interface of all other relevant collections.
    """

    def __new__(cls, iterable):
        return super().__new__(cls, *iterable)


def build_default_registry():
    # We make a copy here just to make sure that eth-abi's default registry is
    # not affected by our custom encoder subclasses
    registry = default_registry.copy()

    registry.unregister("address")
    registry.unregister("bytes<M>")
    registry.unregister("bytes")
    registry.unregister("string")

    registry.register(
        BaseEquals("address"),
        AddressEncoder,
        decoding.AddressDecoder,
        label="address",
    )
    registry.register(
        BaseEquals("bytes", with_sub=True),
        BytesEncoder,
        decoding.BytesDecoder,
        label="bytes<M>",
    )
    registry.register(
        BaseEquals("bytes", with_sub=False),
        ByteStringEncoder,
        decoding.ByteStringDecoder,
        label="bytes",
    )
    registry.register(
        BaseEquals("string"),
        TextStringEncoder,
        decoding.StringDecoder,
        label="string",
    )
    return registry


def get_abi_input_types(abi):
    if "inputs" not in abi and abi["type"] == "fallback":
        return []
    else:
        return [collapse_if_tuple(arg) for arg in abi["inputs"]]


def strip_abi_type(elements):
    if isinstance(elements, ABITypedData):
        return elements.data
    else:
        return elements


@curry
def map_abi_data(normalizers, types, data):
    """
    This function will apply normalizers to your data, in the
    context of the relevant types. Each normalizer is in the format:

    def normalizer(datatype, data):
        # Conditionally modify data
        return (datatype, data)

    Where datatype is a valid ABI type string, like "uint".

    In case of an array, like "bool[2]", normalizer will receive `data`
    as an iterable of typed data, like `[("bool", True), ("bool", False)]`.

    Internals
    ---

    This is accomplished by:

    1. Decorating the data tree with types
    2. Recursively mapping each of the normalizers to the data
    3. Stripping the types back out of the tree
    """
    pipeline = itertools.chain(
        [abi_data_tree(types)],
        map(data_tree_map, normalizers),
        [partial(recursive_map, strip_abi_type)],
    )
    return pipe(data, *pipeline)


@curry
def abi_data_tree(types, data):
    """
    Decorate the data tree with pairs of (type, data). The pair tuple is
    actually an ABITypedData, but can be accessed as a tuple.

    As an example:

    >>> abi_data_tree(types=["bool[2]", "uint"], data=[[True, False], 0])
    [("bool[2]", [("bool", True), ("bool", False)]), ("uint256", 0)]
    """
    return [
        abi_sub_tree(data_type, data_value)
        for data_type, data_value in zip(types, data)
    ]


@curry
def data_tree_map(func, data_tree):
    """
    Map func to every ABITypedData element in the tree. func will
    receive two args: abi_type, and data
    """

    def map_to_typed_data(elements):
        if (
            isinstance(elements, ABITypedData)
            and elements.abi_type is not None
        ):
            return ABITypedData(func(*elements))
        else:
            return elements

    return recursive_map(map_to_typed_data, data_tree)


def reject_recursive_repeats(to_wrap):
    """
    Prevent simple cycles by returning None when called recursively with same
    instance
    """
    to_wrap.__already_called = {}

    @functools.wraps(to_wrap)
    def wrapped(*args):
        arg_instances = tuple(map(id, args))
        thread_id = threading.get_ident()
        thread_local_args = (thread_id,) + arg_instances
        if thread_local_args in to_wrap.__already_called:
            raise ValueError("Recursively called %s with %r" % (to_wrap, args))
        to_wrap.__already_called[thread_local_args] = True
        try:
            wrapped_val = to_wrap(*args)
        finally:
            del to_wrap.__already_called[thread_local_args]
        return wrapped_val

    return wrapped


@reject_recursive_repeats
def recursive_map(func, data):
    """
    Apply func to data, and any collection items inside data
            (using map_collection).
    Define func so that it only applies to the type of value that you want it
    to apply to.
    """

    def recurse(item):
        return recursive_map(func, item)

    items_mapped = map_collection(recurse, data)
    return func(items_mapped)


def abi_sub_tree(
    type_str_or_abi_type: Optional[Union[TypeStr, ABIType]], data_value: Any
) -> ABITypedData:
    if type_str_or_abi_type is None:
        return ABITypedData([None, data_value])

    if isinstance(type_str_or_abi_type, TypeStr):
        abi_type = parse(type_str_or_abi_type)
    else:
        abi_type = type_str_or_abi_type

    # In the two special cases below, we rebuild the given data structures with
    # annotated items
    if abi_type.is_array:
        # If type is array, determine item type and annotate all
        # items in iterable with that type
        item_type_str = abi_type.item_type.to_type_str()
        value_to_annotate = [
            abi_sub_tree(item_type_str, item_value)
            for item_value in data_value
        ]
    elif isinstance(abi_type, TupleType):
        # Otherwise, if type is tuple, determine component types and annotate
        # tuple components in iterable respectively with those types
        value_to_annotate = type(data_value)(
            abi_sub_tree(comp_type.to_type_str(), comp_value)
            for comp_type, comp_value in zip(abi_type.components, data_value)
        )
    else:
        value_to_annotate = data_value

    return ABITypedData([abi_type.to_type_str(), value_to_annotate])


def map_collection(func, collection):
    """
    Apply func to each element of a collection, or value of a dictionary.
    If the value is not a collection, return it unmodified
    """
    datatype = type(collection)
    if isinstance(collection, Mapping):
        return datatype((key, func(val)) for key, val in collection.items())
    if is_string(collection):
        return collection
    elif isinstance(collection, Iterable):
        return datatype(map(func, collection))
    else:
        return collection


def decode_function_input(abi, data):
    data = HexBytes(data)
    selector, params = data[:4], data[4:]
    func = get_function_by_selector(abi, selector)
    names = get_abi_input_names(func)
    types = get_abi_input_types(func)
    codec = ABICodec(build_default_registry())
    decoded = codec.decode_abi(types, params)
    return func, dict(zip(names, decoded))
