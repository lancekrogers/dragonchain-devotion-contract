import time
import httpx
from util import FailureByDesign, Logger

ROPSTEN_SERVICE_URL = "https://slumber-swap-dev.dragonchain.com/ss"


async def wait(delay_in_ms, func):
    time.sleep(delay_in_ms / 1000)
    await func()


def normalize(address):
    return address.lower().strip()


async def get_raw_slumber_score(
    address, retry=1, service_url=ROPSTEN_SERVICE_URL
):
    logger = Logger()
    retry_limit = 4
    backoff_durration_in_ms = 1000
    if not address.startswith("0x"):
        return {
            "address": address,
            "avg": 0,
            "bal": 0,
            "flags": {"dc": False, "ex": False, "sale": False},
            "lb": -1,
            "recovered": 0,
            "ss": 0,
        }
    try:
        url = f"{service_url}/{normalize(address)}"
        async with httpx.AsyncClient() as client:
            fetch_result = await client.get(url)
        if fetch_result.status_code != 200:
            raise FailureByDesign(
                "SUBSYTEM_ERROR",
                "SlumberScore service returned a non 200" "response status.",
            )
        json_response = fetch_result.json()
        if json_response.get("error") is not None:
            del json_response["error"]
            json_response["ss"] = 0
        return json_response
    except Exception as e:
        logger.error(f"ERROR: {e.message}")
        if retry < retry_limit:
            await wait(
                pow(retry, 2) * backoff_durration_in_ms,
                await get_raw_slumber_score(address, retry + 1),
            )
        else:
            raise FailureByDesign(
                "SUBSYTEM_ERROR", "SlumberScore service is unreachable."
            )


async def calculate_score(address):
    """
    Creates a response similar to the party api calculate_score method,
    only there is no adjustment to score value
    """
    slumber_score_response = await get_raw_slumber_score(address)
    raw_score = slumber_score_response.get("ss")
    if raw_score is None:
        raise FailureByDesign(
            "SUBSYSTEM_ERROR",
            "Slumber Service could not find your DDSS. "
            "Please try again later.",
        )
    if raw_score < 0:
        raise FailureByDesign(
            "SUBSYSTEM_ERROR",
            "Slumber Service says your"
            " score is negative? That should not happen.",
        )
    slumber_score_response["rawScore"] = raw_score
    slumber_score_response["adjustedScore"] = raw_score
    slumber_score_response["claimed"] = 0
    slumber_score_response["staked"] = 0
    slumber_score_response["leased"] = 0

    del slumber_score_response["ss"]

    return slumber_score_response
