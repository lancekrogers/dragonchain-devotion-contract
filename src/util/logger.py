import os
import sys
import json


class Logger:
    def __init__(self):
        self.log_level = os.getenv("LOG_LEVEL")

    def info(self, message):
        if self.log_level != "OFF":
            sys.stderr.write(f"{json.dumps(message)}\n")

    def trace(self, message):
        if self.log_level != "OFF":
            sys.stderr.write(f"{json.dumps(message)}\n")

    def error(self, message):
        if self.log_level != "OFF":
            sys.stderr.write(f"{json.dumps(message)}\n")

    def debug(self, message):
        if self.log_level != "OFF":
            sys.stderr.write(f"{json.dumps(message)}\n")
