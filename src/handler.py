import json
from lib.methods import (
    register,
    cancel,
    devote,
    watcher,
    claim_quipu_id,
    generate_totals,
)
from lib.clients import HeapMemory, DragonchainClient
from util.logger import Logger

# from lib.schemas import SchemaValidator


async def handler(event):
    try:
        # SchemaValidator.is_valid_event_payload(event) # Throws INVALID_SCHEMA

        dragonchain = DragonchainClient()
        await dragonchain.initialize()

        logger = Logger()

        heap_memory = HeapMemory(dragonchain)

        # Unpack event
        header = event["header"]
        payload = event["payload"]
        try:
            method = payload["method"]
            parameters = payload["parameters"]
        except KeyError:
            logger.info("Watcher transaction")
            method = "watcher"
            parameters = payload["transactions"]
            logger.info(json.dumps(parameters))
        heap_memory.set("invoker", header["invoker"])

        valid_methods = {
            "register": register,
            "cancel": cancel,
            "devote": devote,
            "watcher": watcher,
            "claimQuipuId": claim_quipu_id,
            "generateTotals": generate_totals,
        }

        status = await valid_methods.get(method, "Invalid method")(
            heap_memory, parameters
        )

        logger.info(f"METHOD STATUS -> {status}")

        new_heap_state = heap_memory.get_new_state()

        logger.info(json.dumps(new_heap_state))

        return new_heap_state
    except TypeError as e:
        return {"error": str(e)}
