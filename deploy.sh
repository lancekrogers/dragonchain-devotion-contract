#!/usr/bin/env bash

# Script to easily deploy dragonchain contracts 

# Config Variables
ECR_REPO_NAME=devotion
REPO_PATH=381978683274.dkr.ecr.us-west-2.amazonaws.com
START_SCRIPT="python src/main.py"

deployContract () {
	imageUri=$1

	token=`aws ecr get-authorization-token | jq -r '.authorizationData[0].authorizationToken'`

	read -p "Would you like to update an existing contract? [Y|n] " update	

	if [[ -z "$update" || "$update" =~ ^(yes|y|Y) ]]; then
		read -p "What is the contract id? " contractId
		echo Updating contract: $contractId 
		echo 

		dctl c u $contractId -I $imageUri -r $token
	else
		read -p "Would you like to create a new contract? [Y|n]" new
		if [[ -z "$new" || "$new" =~ ^(yes|y|Y) ]]; then
			read -p "What would you like to call the new contract? (transaction_type) " contractName
			
			while [[ "$contractName" == "" ]]; do 
				echo "Contract Name (transaction_type) is required"
				echo 
				read -p "What would you like to call the new contract? (transaction_type) " contractName
			done
				
			
			echo Deploying new contract
			echo
			
			
			dctl c c $contractName  $imageUri $START_SCRIPT -r $token
		else
			echo Goodbye.
		fi
	fi
}



deploy () {
	read -p "Please enter a version number " version 
	if [[ "$version" == "" ]]; then
		version=$[RANDOM%433494437+1]
		echo
		echo Random version used
	fi
	
	echo
	
	echo Version Number: $version
	echo

		
	sudo $(aws ecr get-login --no-include-email --region us-west-2)

	sudo docker build -t $ECR_REPO_NAME .

	sudo docker tag $ECR_REPO_NAME:latest $REPO_PATH/$ECR_REPO_NAME:$version 
	
	sudo docker push $REPO_PATH/$ECR_REPO_NAME:$version  

	deployContract $REPO_PATH/$ECR_REPO_NAME:$version  

}



deploy "$@"
