FROM python:3.7.2-alpine3.9

WORKDIR /home/app

COPY requirements.txt .

RUN apk update && apk add libressl-dev postgresql-dev libffi-dev gcc musl-dev python3-dev 


RUN  pip install --no-cache-dir Cython

RUN  python -m pip install --upgrade pip && \
     python -m pip install -r requirements.txt

COPY . .
