# Devotion Dragonchain Contract

## Transaction type: contract name 

## Methods

  - register [(example payload)](local/payloads/register.json)
  - cancel [(example payload)](local/payloads/cancel.json)
  - claimQuipuId [(example payload)](local/payloads/claim_quipu_id.json)
  - generateTotals [(example payload)](local/payloads/generateTotals.json)


## Heap structure
```
├── devotions
│   ├── devoted (contains devoted devotions)
│   │   ├── general (quipuId)
│   │   │   ├── 0x0e8e1b8dbbdd2bf56e8dcc6dc88f2c8c8a0f2a48
│   │   │   ├─  0x...... 
│   │   │   └── totals (contains the total of drgn, time and fees for the indiviual quipuId)
│   │   └── registered (Contains registered devotions)
│   │       └── 0x0e8e1b8dbbdd2bf56e8dcc6dc88f2c8c8a0f2a48
│   │       └── 0x.... 
└── quipu
    └── registry (Contains all registered quipuId and ther keccak hashes)
```
