# Devotion Dragonchain Contract

This contract enables DRGN holders to devote their time to a unique quipuId.  QuipuId's are unique Identifiers that can  be used to represent anything.  These two components together can be used for a sort of weighted voting. 

# Methods

- register:
	- Send the requested amount of time, address the DRGN will be sent from and quipuId to devote to
	- Returns the amount of DRGN you must send to devote the requested time
- cancel:
	- cancels a registered devotion
- claimQuipuId:
	- Registers a quipuId
- generateTotals:
	- Recalculates totals for a given quipuId

# Overview of devotion process
0. A unique quipuId is claimed using the claimQuipuId method 
1. Devotions are registered with the register method to a claimed quipuId
2. Registrant sends the amuont of DRGN returned by the register method to the Ethereum Devotion contract
3. The Ethereum watcher contract detects the DRGN sent to the Ethereum Devotion contract
4. The Ethereum watcher sends the detected transaction to the Devotion Dragonchain contract
5. The Devotion Dragonchain contract looks for a registed devotion from the token senders address for the exact amount of DRGN sent
6. If a match is found the registered devotion becomes devoted and the sent tokens are returned to their sender minus a fee (via triggering an Ethereum Publisher contract).  If no match is found, the DRGN are returned to the sender minus a fee and the senders TIME is not devoted to a quipuId (via triggering an Ethereum Publisher contract).
7. If the coins are returned succesffully, the Ethereum Watcher contract will detect the Ethereum Devotion contract invocation.  This will be sent to the Devotion Dragonchain contract.
8. If there is a matching deovted devotion, the transaction Id from the contract invoking transaction will be added as a receipt to the devoted devotion.

## Linting

After pip installing requirements.txt run the below command to lint  new
source code 
```black --line-length=79 src/```
